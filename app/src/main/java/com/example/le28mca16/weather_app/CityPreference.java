package com.example.le28mca16.weather_app;

/**
 * Created by LE28MCA16 on 1/5/18.
 */

import android.app.Activity;
import android.content.SharedPreferences;
public class CityPreference {

    SharedPreferences prefs;

    public CityPreference(Activity activity){
        prefs = activity.getPreferences(Activity.MODE_PRIVATE);
    }

    // If the user has not chosen a city yet, return
    // Sydney as the default city
    String getCity(){
        return prefs.getString("city", "Sydney, AU");
    }

    void setCity(String city){
        prefs.edit().putString("city", city).commit();
    }


}
